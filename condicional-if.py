#condicional if

edad = 17
nombre = "Jose"

if edad >= 18:
    print("eres mayor de edad")

#condicional if-else
if edad >=18:
    print("eres mayor de edad...")
else:
    print("eres un chaval aun...")

if edad == 18 and nombre=="Jose":
    print("Bienvenido estas en la lista...")

#condicional elif
x = "diciembre"
if x == "octubre":
    print("Es octubre")
elif x == "noviembre":
    print("Es noviembre")
elif x == "diciembre":
    print("Es navidad")
else:
    print("cualquier otro mes")

#operador ternario
y = 5
resultado ="Es 5" if y == 5 else "No es 5"
print(resultado)