#lista (los valor dentro de una lista guardan 
#posiciones iniciado siempre desde la posicion 0)

lista = [1,2,3,4]
#imprimir lista completa
print(lista)
#como acceder a un elemento especifico
print( lista[2] )

lista2 = list("5678")
print(lista2)
print(lista2[0])

lista3 = [1,"Hola",3.60,[1,2,3]]
print(lista3)
print(lista3[3][1])

#modificar listas
a = [90,"python",3.87]
a[2] =  1
print(a)

#eliminar contenidos de lista
del a[1]
print(a)


#tuplas-es parecida a la lista con salvedad de que los datos son inmutables
tupla1 = (1,2,3,4)
print(tupla1)
print(tupla1[3])

#set
instrumentos = set(["guitarra","bateria","bajo"])
print(instrumentos)
#instrumentos[0]= "violin"



