#conversion implicita
a = 1 #int
b = 2.3 #float

a = a + b
print(a)
print( type(a) )

#conversion explicita
#convetir a float float(int), float(str)
#convetir a int int(float), int(str)
#convetir a str str(float), str(int)
#convertir set a lista
numeros = {1,2,3,4}
lista = list(numeros)
print(type(numeros))
print(type(lista))

#convertir lista a set
lista1 = ["Python","Mola"]
contenido = set(lista1)
print(type(lista1))
print(type(contenido))
