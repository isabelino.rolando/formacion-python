#tipos de variables
#entero - int
numero = 12
print(numero)
#para saber el tipo de variable en python usar type()
print(type(numero))
#convertir un decimal a entero int(aqui el valor decimal)
conversion = int(15.9)
print(conversion)

#decimal - float
numero2 = 0.10088
print(numero2)
print(type(numero2))
#convertir un entero a decimal float(aqui el valor entero)
conversion2 = float(100)
print(conversion2)

#numero complejo
a = 1 + 3j
b = 4 + 1j
suma = a+b
print(suma)

#cadena string - str
#va con doble comilla "valor" o con comillas simple 'valor'

s = "Esto es una cadena"
print(s)
print(type(s))

t = 'Cadena con comilla simple'
print(t)
print(type(t))
#agregar comillas dentro de una cadena
p = "Comillas dentro \"comillas \" de un caracter"
print(p)
#salto de linea en string
m = "Primera linea \n Segunda linea"
print(m)

#concatenar variables

x = 5
s = "El numero es: " + str(x)
print(s)

y = 6
t = "El %dsiguiente %dvalor es: %d " % (10,x,y)
print(t)

r = "Los numeros son {a} y {b} ".format(a=10,b=15)
print(r)

print(f"El valor de x es :{x}, el valor de y es: {y}")


#booleanos logicos - boolean

x = True
print(x)

y = False
print(y)

#evaluar expresiones 
print( 1 > 0  )#True
print( 1 <= 9 )#True
print( 9 == 8 )#False
#funcion de evalucion de valor
#si existe valor pasa True bool(aqui va el valor)
#si no hay valor pasa False bool(aqui va el valor)
print(bool("hola"))
print(bool([]))

verdadero = int(True)
falso = int(False)
print(f"{verdadero} {falso}")

#ejemplo de fechas en python
from datetime import date,time,datetime
print(date.today)
dt = datetime.now()
print(type(dt))
print(dt.year)
print(dt.month)
print(dt.day)
print(dt.hour)
print(dt.minute)
print(dt.second)
print(dt.microsecond)