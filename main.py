
saludo = "Hola mundo python desde visual studio code"
print(saludo)

#comentarios en una sola linea

'''
esto es un comentario
de varias lineas en python
con comillas simples 
'''

""" 
esto es un comentario 
de varias lineas en python con comillas dobles
"""

#identacion y bloques de codigo
if False:
    print("ingreso a este bloque de codigo")

   
#se pueden tener varias variables en una sola linea
#con el ;(punto y coma)

x = 10 ; y = 5 ; nombre="Jose"