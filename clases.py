
class Perro:
    #atributo o variable
    animal= "perro"
    raza=""
    nombre=""
    edad=0
    color=""

    #constructor
    def __init__(self,raza,nombre,edad,color):
        self.raza = raza
        self.nombre= nombre
        self.edad = edad
        self.color= color


    #metodos o funciones
    def ladra(self):
        print("wooff wooff wooff")

    def comer(self):
        print("comiendo")    

#instanciar la clase perro
# crear un objeto de la clase perro        


perro1= Perro("caniche","puchi",4,"blanco")
print(perro1.animal)
print(perro1.comer())
print(perro1.ladra())

#perro2 = Perro()
#print(perro2.animal)
#print(perro2.comer())
#print(perro2.ladra())


