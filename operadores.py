#operadores de asignacion
a = 7; b=2
print("operadores de asignacion")
x=a; x += b; print("x+=",x)
x=a; x -= b; print("x-=",x)
x=a; x *= b; print("x*=",x)
x=a; x /= b; print("x/=",x)
x=a; x %= b; print("x%=",x)

#operadores aritmeticos
j=10;k=3
print("operadores aritmeticos")
print("j+k=",j+k)#suma
print("j-k=",j-k)#resta
print("j*k=",j*k)#multiplicacion
print("j/k=",j/k)#division
print("j%k=",j%k)#modulo
print("j**k=",j**k)#potencia
print("j//k=",j//k)#division entera

#operadores relacionales
m=2;n=3
print("operadores relacionales")
print(" m == n =",m == n)#igualdad False
print(" m != n =",m != n)#distinto True
print(" m > n =",m > n)#mayor que False
print(" m < n =",m < n)#menor que True
print(" m >= n =",m > n)#mayor o igual que False
print(" m <= n =",m <= n)#menor o igual que True

#operadores logicos, and, or, not
#and:devuelve true si ambos elementos son verdaderos
#or: devuelve true si al menos un lementos es verdadero
#not: devuelve lo opuesto, true si es false y viceversa
print("operadores logicos")
print("and")
print(True and True)
print(True and False)
print(False and True)
print(False and False)
print("or")
print(True or True)
print(True or False)
print(False or True)
print(False or False)
print("not")
print(not True)
print(not False)
