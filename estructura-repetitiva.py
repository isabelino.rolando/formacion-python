#bucle while-esto se repite mientras la condicion sea true
x = 5
while x < 0:
    print(x)
    x -= 1 #lo mismo a x = x-1


#bucle while else
y = 5
while y<0:
    print(y)
    y -= 1
else:
    print("el bucle ha finalizado")    

#bucle for
#range(inicio,fin,salto)
for i in range(1,11):
    print(i) 

alumnos=["Maria","Jose","Marcelo","Juan"]
for n in alumnos:
    print(n)

for i in (1,2,3,4,5,6,7):
    print(i)

for t in range(6):
    print(t)

for i in range(5,20,2):
    print(i)