#variables
#a tener en cuenta que las mayusculas y minusculas 
Nombre="Jose"
nombre="Carlos"
print(Nombre)

#el nombre de variable no puede empezar por un numero
#no se permiten el uso de guiones -
#no se permiten espacios

#no valido
'''
12cuentas= 10
-nombre = 11
nombre-completo="Juan Guti"
cuenta nomina=124545
'''
#valido para declarar variables
_variable = 10
varia_ble = 5
banca36 = 4545
variable1 = 60
variaBle1 = 50

#en python no se puede utilizar las palabras reservadas
#como ver todas las palabras reservadas

import keyword
print(keyword.kwlist)

#Uso de parentesis en operaciones aritmeticas */+-
num1 = 10
num2 = (num1*3 - 3)**(10-2+3)
print(num2)