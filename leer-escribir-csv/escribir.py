import csv
#creacion de lista de clientes 
clientes = [
    ['Maria','Perez','mp@email.com','Madrid'],
    ['Juan','Ramos','jr@email.com','Madrid'],
    ['Carlos','Ruiz','cr@email.com','Madrid'],
    ['Pedro','Alcaraz','pa@email.com','Madrid'],
]
#llamada al metodo open donde se define el path
#donde va el archivo csv creado
#with open('leer-escribir-csv/clientes.csv','w',newline='') as file:
with open('C://Users//dell//Desktop//clientes.csv','w',newline='') as file:
    #definimos metodo de escritura y delimitador
    writer = csv.writer(file,delimiter=',')
    #generamos la escritura con la lista llamada clientes
    writer.writerows(clientes)