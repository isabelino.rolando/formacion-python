#funciones o metodos

#metodo normal
def saludo():
    print("hola esto es un metodo")
saludo()

#metodo con argumento
def saludar(nombre,apellido):
    print("Nombre: "+nombre +" Apellido: "+apellido)
saludar("Pedro","Ramos")

#metodo con retorno
def ciudad(nombre):
    return "La ciudad es: "+nombre
print( ciudad("Valencia") )

def suma(a,b):
    return a+b

print("La suma de 12 y 6 es: "+ str( suma(12,6) ) )


def suma1(*args):
    s = 0
    for arg in args:
        s += arg
    return s
print( suma1(1,3,17,4))
a=10 
def suma2(**kwargs):
    t=0
    for key, value in kwargs.items():
        print(key,"=",value)
        t += value
    return t
print( suma2(a=3,b=10,c=3) )

print(a)


'''def suma3( a:int, b:int )-> str:
    return "esto es la suma "+str( (a+b) )'''
def suma3( a:int, b:int ) -> int:
    return "esto es la suma "+str( (a+b) )

print(suma3(7,3))    

#funcion lambda
sumatoria = lambda a,b : a+b
print(sumatoria(3,6))

#funcion interna input es entrada de valores por consola
print("Inicio de programa")
nombre = input("Por favor ingrese su nombre aqui: ")
print("Bienvenido "+str(nombre))